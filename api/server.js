import jsonServer from 'json-server';
import { db } from '../db.js';

const server = jsonServer.create();
const router = jsonServer.router(db);
const middlewares = jsonServer.defaults({ readOnly: true });
const port = process.env.PORT || 3000;

server.use(middlewares);
server.use(router);
server.listen(port);
