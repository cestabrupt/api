import { createRequire } from 'node:module';
const require = createRequire(import.meta.url);

const naufrages = require('./src/naufrages.json');
const enferTxt = require('./src/enfer-txt.json');
const robotRimbaud = require('./src/robot-rimbaud.json');

export const db = {
  naufrages,
  "enfer.txt": enferTxt,
  "robot-rimbaud": robotRimbaud
};
