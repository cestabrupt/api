﻿# A.P.I. (Abrüpt pour Pirates Internationalistes)

Les API d'Abrüpt sont disponibles sur [api.abrupt.cc](https://api.abrupt.cc).

## API

Nous mettons à disposition [des textes au format JSON](/api), sous la forme d'API accessibles à travers la méthode HTTP `GET`.

## Technicité

On utilise [JSON Server](https://github.com/typicode/json-server) pour fournir ces API.

## Liberté

Nous n'utilisons pour ces API que des textes dédiés au domaine public grâce à la [licence Creative Commons Zero](LICENSE-TXT), ou se trouvant déjà dans le domaine public. Toutes modifications de ces derniers sont placées également sous cette même licence.

Le code source, au-delà des librairies utilisées, permettant le fonctionnement de ces API est quant à lui également dédié au [domaine public volontaire](LICENSE).

(*L'information veut être libre.*)

